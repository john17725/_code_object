step_1_quicksort:
	gcc -c -m32 _QuickSort_.c -o  output_quicksort.o
step_1_la_costes:
	gcc -c -m32 _la_costes_.c -o  output_la_costes.o
step_1_li_mem:
	gcc -c -m32 li_mem.c -o  output_li_mem.o
step_2_quicksort:
	readelf -S output_quicksort.o
step_2_la_costes:
	readelf -S output_la_costes.o
step_2_li_mem:
	readelf -S output_li_mem.o
step_3_quicksort:
	gcc -m32 output_quicksort.o -o output_quicksort
step_3_la_costes:
	gcc -m32 output_la_costes.o -o output_la_costes -lm
step_3_li_mem:
	gcc -m32 output_li_mem.o -o output_li_mem 
step_4_quicksort:
	readelf -S output_quicksort
step_4_la_costes:
	readelf -S output_la_costes
step_4_li_mem:
	readelf -S output_li_mem
step_5_quicksort:
	objdump -d output_quicksort.o -j .text
step_5_la_costes:
	objdump -d output_quicksort.o -j .text
step_5_li_mem:
	objdump -d output_li_mem.o -j .text
step_6_quicksort:
	gdb -batch -ex 'file output_quicksort' -ex 'disassemble main'
step_6_la_costes:
	gdb -batch -ex 'file output_la_costes' -ex 'disassemble main'
step_6_li_mem:
	gdb -batch -ex 'file output_li_mem' -ex 'disassemble main'
clean:
	rm *.o
	rm output_quicksort
	rm output_la_costes
	rm output_li_mem