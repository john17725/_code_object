#include <stdio.h>
#include <stdlib.h>
#define LENGT_ARR 30
void quicksort(int *target, int left, int right);
void charge_arr(int *target);
void show_arr_(int *target);

int main() {
  int i, array[LENGT_ARR];
  charge_arr(array);
  show_arr_(array);
  quicksort(array, 0, LENGT_ARR-1);   
  show_arr_(array);
  return 0;
}

void quicksort(int *target, int left, int right) {
  if(left >= right) return;
  int i = left, j = right;
  int tmp, pivot = target[i];
  for(;;) {
    while(target[i] < pivot) i++;
    while(pivot < target[j]) j--;
    if(i >= j) break;
    tmp = target[i]; target[i] = target[j]; target[j] = tmp;
    i++; j--;
  }
  quicksort(target, left, i-1);
  quicksort(target, j+1, right);
}

void charge_arr(int *target){
  for (int i = 0; i < LENGT_ARR; i++)
  {
    target[i] = rand();
  }
}

void show_arr_(int *target){
  for (int i = 0; i < LENGT_ARR; i++)
  {
    printf("[%d][ %d ]\n",i,target[i]);
  }
}